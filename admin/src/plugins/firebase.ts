import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  //    config
  apiKey: 'AIzaSyBw71oCHvBCUhkRye13adSC4RXEvTDszUg',
  authDomain: 'web-apps-roadmap.firebaseapp.com',
  projectId: 'web-apps-roadmap',
  storageBucket: 'web-apps-roadmap.appspot.com',
  messagingSenderId: '754344780727',
  appId: '1:754344780727:web:2b085441588cf4592cbe32',
  measurementId: 'G-93TE18DQ8K'
}

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app);


export { auth, db, storage };