import { createRouter, createWebHistory } from 'vue-router'
import { useAuthStore } from '@/stores/auth'
const routes = [
  {
    path: '/',
    name: 'dashboard',
    meta: { requiresAuth: true },
    component: () => import('../views/DashboardView.vue')
  },
  {
    path: '/messages',
    name: 'messages',
    meta: { requiresAuth: true },
    component: () => import('../views/messages/MessageInbox.vue')
  },
  {
    path: '/properties',
    name: 'properties',
    meta: { requiresAuth: true },
    component: () => import('../views/properties/PropertiesPage.vue')
  },
  {
    path: '/gallery',
    name: 'gallery',
    meta: { requiresAuth: true },
    component: () => import('../views/ImageGallery.vue')
  },
  {
    path: '/properties/create',
    name: 'create',
    meta: { requiresAuth: true },
    component: () => import('../views/properties/CreateProperty.vue')
  },

  // auth
  {
    path: '/auth/login',
    name: 'login',
    component: () => import('../views/auth/LoginView.vue')
  },
  {
    path: '/auth/register/:type',
    name: 'register',
    props: true,
    component: () => import('../views/auth/RegisterView.vue')
  },
  {
    path: '/auth',
    name: 'auth',
    component: () => import('../views/auth/AuthView.vue')
  },
  {
    path: '/auth/reset',
    name: 'reset',
    component: () => import('../views/auth/ResetView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
  scrollBehavior: function (to, _from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { el: to.hash, behavior: 'smooth' }
    } else {
      // console.log("moving to top of the page");
      window.scrollTo(0, 0)
    }
  }
})

router.beforeEach((to, from, next) => {
  const store = useAuthStore()

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.isLoggedIn == true) {
      next()
      return
    }
    next('/auth')
  } else {
    next()
  }
})

export default router
