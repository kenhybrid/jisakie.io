import { defineStore } from 'pinia'
import { ref } from 'vue'
import { db, storage, auth } from '../plugins/firebase'
import router from '@/router'
import {
  onAuthStateChanged,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signOut
} from 'firebase/auth'

interface Admin {
  password: string
  email: string
}

interface Error {
  message: string
  state: boolean
}

export const useAuthStore = defineStore(
  'jisakie-admin-auth',
  () => {
    // auth info
    const isLoading = ref<boolean>(false)
    const authError = ref<Error>({
      message: '',
      state: false
    })
    const isLoggedIn = ref<boolean>(false)

    const admin = ref<Admin>({
      email: '',
      password: ''
    })

    // METHODS

    const loginAdmin = async ({ email, password }: Admin) => {
      isLoading.value = true
      try {
        //    login logic
        const res = await signInWithEmailAndPassword(auth, email, password)

        // set user value
        isLoggedIn.value = true
        isLoading.value = false

        // redirect on success
        setTimeout(() => {
          router.push('/')
        }, 300)
      } catch (error: any) {
        isLoading.value = false

        authError.value.message = error.response.data.err.message
        authError.value.state = true

        setTimeout(() => {
          authError.value.message = ''
          authError.value.state = false
        }, 3000)
      }
    }

    const resetAdmin = async ({ email }: any) => {
      isLoading.value = true
      try {
        // reset logic
        await sendPasswordResetEmail(auth, email)

        // redirect
        setTimeout(() => {
          isLoading.value = false

          router.push('/auth/login')
        }, 500)
      } catch (error: any) {
        isLoading.value = false

        authError.value.message = error.response.data.err.message
        authError.value.state = true

        setTimeout(() => {
          authError.value.message = ''
          authError.value.state = false
        }, 3000)
      }
    }
    const logoutAdmin = async () => {
      try {
        //    signout logic
        await signOut(auth)

        setTimeout(() => {
          router.push('/auth/login')
          isLoggedIn.value = false
        }, 300)
        localStorage.removeItem('authstore-coastview')
      } catch (error: any) {
        console.log(error)
      }
    }
    const authState = async () => {
      try {
        // authsatate logic
        await onAuthStateChanged(auth, (user) => {
          if (user) {
            // User is signed in, see docs for a list of available properties
            // https://firebase.google.com/docs/reference/js/auth.user
            const uid = user.uid

            // ...
          } else {
            router.push('/auth/login')
          }
        })
      } catch (error: any) {
        console.log(error)
      }
    }

    return {
      isLoggedIn,
      isLoading,
      authError,
      logoutAdmin,
      resetAdmin,
      loginAdmin,
      authState,
      admin
    }
  },
  {
    persist: true
  }
)
