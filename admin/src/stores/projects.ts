import { defineStore } from 'pinia'
import { ref } from 'vue'
import { db, storage, auth } from '../plugins/firebase'
import router from '@/router'
import { uploadBytes, ref as Ref, getDownloadURL, deleteObject } from 'firebase/storage'
import { addDoc, collection, deleteDoc, doc, getDocs, query, where } from 'firebase/firestore'

interface Project {
  password?: string
  email: string
}

interface Land {
  images: string[]
  imagerefs: string[]
  id: string
  size: string
  price: string
  location: string
  type: string
  description: string
  date: Date
}

interface Apartment {
  images: string[]
  features: string[]
  imagerefs: string[]
  id: string
  title: string
  price: string
  location: string
  type: string
  description: string
  date: Date
}

interface Project {
  images: string[]
  features: string[]
  imagerefs: string[]
  id: string
  title: string
  location: string
  type: string
  description: string
  date: Date
}
interface Bungalow {
  images: string[]
  features: string[]
  imagerefs: string[]
  id: string
  title: string
  price: string
  location: string
  bedroom: string
  bathroom: string
  kitchen: string
  squaremeters: string
  type: string
  description: string
  date: Date
}

interface Villa {
  images: string[]
  features: string[]
  imagerefs: string[]
  id: string
  title: string
  price: string
  bedroom: string
  bathroom: string
  kitchen: string
  squaremeters: string
  location: string
  type: string
  description: string
  date: Date
}

interface LandInput {
  images: File[]
  size: string
  price: string
  location: string
  type: string
  description: string
}
interface Error {
  message: string
  state: boolean
}

export const useProjectStore = defineStore(
  'coastview-projects-admin',
  () => {
    // project info
    const isLoading = ref<boolean>(false)
    const uploadError = ref<Error>({
      message: '',
      state: false
    })

    const lands = ref<Land[]>([])
    const apartments = ref<Apartment[]>([])

    const bungalows = ref<Bungalow[]>([])
    const projects = ref<Project[]>([])
    const villas = ref<Villa[]>([])
    const gallery = ref<any[]>([])

    // METHODS
    const createLand = async ({ images, size, price, location, type, description }: LandInput) => {
      isLoading.value = true
      try {
        // image upload
        const urls = []
        const imagerefs = []

        for (let i = 0; i < images.length; i++) {
          const imagename = new Date().toISOString()
          await uploadBytes(Ref(storage, 'images/land/' + imagename), images[i])
          const link = await getDownloadURL(Ref(storage, 'images/land/' + imagename))
          urls.push(link)
          imagerefs.push(imagename)
        }

        const data = await addDoc(collection(db, 'properties'), {
          images: urls,
          size,
          price,
          location,
          type,
          description,
          imagerefs,
          date: new Date().toISOString()
        })

        const object: any = {
          id: data.id,
          images: urls,
          size,
          price,
          location,
          imagerefs,
          type,
          description,
          date: new Date().toISOString()
        }

        lands.value.push(object)
        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const createApartment = async ({
      images,
      title,
      price,
      location,
      type,
      description,
      features
    }: any) => {
      isLoading.value = true
      try {
        // image upload
        const urls = []
        const imagerefs = []

        for (let i = 0; i < images.length; i++) {
          const imagename = new Date().toISOString()
          await uploadBytes(Ref(storage, 'images/apartment/' + imagename), images[i])
          const link = await getDownloadURL(Ref(storage, 'images/apartment/' + imagename))
          urls.push(link)
          imagerefs.push(imagename)
        }

        const data = await addDoc(collection(db, 'properties'), {
          images: urls,
          title,
          price,
          location,
          type,
          description,
          features: features.split(','),
          imagerefs,
          date: new Date().toISOString()
        })

        const object: any = {
          id: data.id,
          images: urls,
          title,
          features: features.split(','),
          price,
          location,
          imagerefs,
          type,
          description,
          date: new Date().toISOString()
        }

        apartments.value.push(object)
        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const createProject = async ({ images, title, location, type, description, features }: any) => {
      isLoading.value = true
      try {
        // image upload
        const urls = []
        const imagerefs = []

        for (let i = 0; i < images.length; i++) {
          const imagename = new Date().toISOString()
          await uploadBytes(Ref(storage, 'images/project/' + imagename), images[i])
          const link = await getDownloadURL(Ref(storage, 'images/project/' + imagename))
          urls.push(link)
          imagerefs.push(imagename)
        }
        console.log({
          images: urls,
          title,
          location,
          type,
          description,
          features: features.split(','),
          imagerefs,
          date: new Date().toISOString()
        })

        const data = await addDoc(collection(db, 'properties'), {
          images: urls,
          title,
          location,
          type,
          description,
          features: features.split(','),
          imagerefs,
          date: new Date().toISOString()
        })

        const object: any = {
          id: data.id,
          images: urls,
          title,
          features: features.split(','),

          location,
          imagerefs,
          type,
          description,
          date: new Date().toISOString()
        }

        projects.value.push(object)
        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const createVilla = async ({
      images,
      title,
      price,
      bedroom,
      bathroom,
      kitchen,
      squaremeters,
      location,
      type,
      description,
      features
    }: any) => {
      isLoading.value = true
      try {
        // image upload
        const urls = []
        const imagerefs = []

        for (let i = 0; i < images.length; i++) {
          const imagename = new Date().toISOString()
          await uploadBytes(Ref(storage, 'images/villa/' + imagename), images[i])
          const link = await getDownloadURL(Ref(storage, 'images/villa/' + imagename))
          urls.push(link)
          imagerefs.push(imagename)
        }

        const data = await addDoc(collection(db, 'properties'), {
          images: urls,
          title,
          price,
          bedroom,
          bathroom,
          kitchen,
          squaremeters,
          location,
          type,
          description,
          features: features.split(','),
          imagerefs,
          date: new Date().toISOString()
        })

        const object: any = {
          id: data.id,
          images: urls,
          title,
          features: features.split(','),
          price,
          bedroom,
          bathroom,
          kitchen,
          squaremeters,
          location,
          imagerefs,
          type,
          description,
          date: new Date().toISOString()
        }

        villas.value.push(object)
        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const createBungalow = async ({
      images,
      title,
      price,
      bedroom,
      bathroom,
      kitchen,
      squaremeters,
      location,
      type,
      description,
      features
    }: any) => {
      isLoading.value = true
      try {
        // image upload
        const urls = []
        const imagerefs = []

        for (let i = 0; i < images.length; i++) {
          const imagename = new Date().toISOString()
          await uploadBytes(Ref(storage, 'images/bungalow/' + imagename), images[i])
          const link = await getDownloadURL(Ref(storage, 'images/bungalow/' + imagename))
          urls.push(link)
          imagerefs.push(imagename)
        }

        const data = await addDoc(collection(db, 'properties'), {
          images: urls,
          title,
          price,
          bedroom,
          bathroom,
          kitchen,
          squaremeters,
          location,
          type,
          description,
          features: features.split(','),
          imagerefs,
          date: new Date().toISOString()
        })

        const object: any = {
          id: data.id,
          images: urls,
          title,
          features: features.split(','),
          price,
          bedroom,
          bathroom,
          kitchen,
          squaremeters,
          location,
          imagerefs,
          type,
          description,
          date: new Date().toISOString()
        }

        bungalows.value.push(object)
        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const createGallery = async ({
      image,

      description
    }: any) => {
      isLoading.value = true
      try {
        // image upload

        const imagename = new Date().toISOString()
        await uploadBytes(Ref(storage, 'images/gallery/' + imagename), image)
        const link = await getDownloadURL(Ref(storage, 'images/gallery/' + imagename))

        const data = await addDoc(collection(db, 'gallery'), {
          image: link,
          description,
          imagerefs: imagename,
          date: new Date().toISOString()
        })

        const object: any = {
          id: data.id,
          image: link,
          description,
          imagerefs: imagename,
          date: new Date().toISOString()
        }

        gallery.value.push(object)
        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }

    const loadLands = async () => {
      isLoading.value = true
      try {
        const landQuery = query(collection(db, 'properties'), where('type', '==', 'land'))

        const landSnapshot = await getDocs(landQuery)
        const arrLands: any[] = []
        landSnapshot.forEach((doc) => {
          const obj = {
            id: doc.id,
            size: doc.data().size,
            location: doc.data().location,
            price: doc.data().price,
            images: doc.data().images,
            imagerefs: doc.data().imagerefs,
            type: doc.data().type,
            date: doc.data().date,
            description: doc.data().description
          }
          arrLands.push(obj)
          console.log(obj)
        })
        lands.value = arrLands

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const loadApartments = async () => {
      isLoading.value = true
      try {
        const landQuery = query(collection(db, 'properties'), where('type', '==', 'apartment'))

        const apartmentSnapshot = await getDocs(landQuery)
        const arrApartments: any[] = []
        apartmentSnapshot.forEach((doc) => {
          const obj = {
            id: doc.id,
            title: doc.data().title,
            location: doc.data().location,
            price: doc.data().price,
            images: doc.data().images,
            type: doc.data().type,
            imagerefs: doc.data().imagerefs,
            features: doc.data().features,
            date: doc.data().date,
            description: doc.data().description
          }
          arrApartments.push(obj)
          console.log(obj)
        })
        apartments.value = arrApartments

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const loadProjects = async () => {
      isLoading.value = true
      try {
        const landQuery = query(collection(db, 'properties'), where('type', '==', 'project'))

        const projectSnapshot = await getDocs(landQuery)
        const arrProject: any[] = []
        projectSnapshot.forEach((doc) => {
          const obj = {
            id: doc.id,
            title: doc.data().title,
            location: doc.data().location,
            images: doc.data().images,
            imagerefs: doc.data().imagerefs,
            type: doc.data().type,
            features: doc.data().features,
            date: doc.data().date,
            description: doc.data().description
          }
          arrProject.push(obj)
          console.log(obj)
        })
        projects.value = arrProject

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const loadBungalows = async () => {
      isLoading.value = true
      try {
        const landQuery = query(collection(db, 'properties'), where('type', '==', 'bungalow'))

        const projectSnapshot = await getDocs(landQuery)
        const arrProject: any[] = []
        projectSnapshot.forEach((doc) => {
          const obj = {
            id: doc.id,
            title: doc.data().title,
            location: doc.data().location,
            price: doc.data().price,
            bedroom: doc.data().bedroom,
            bathroom: doc.data().bathroom,
            type: doc.data().type,
            kitchen: doc.data().kitchen,
            squaremeters: doc.data().squaremeters,
            images: doc.data().images,
            imagerefs: doc.data().imagerefs,
            features: doc.data().features,
            date: doc.data().date,
            description: doc.data().description
          }
          arrProject.push(obj)
          console.log(obj)
        })
        bungalows.value = arrProject

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const loadVillas = async () => {
      isLoading.value = true
      try {
        const landQuery = query(collection(db, 'properties'), where('type', '==', 'villa'))

        const villaSnapshot = await getDocs(landQuery)
        const arrVilla: any[] = []
        villaSnapshot.forEach((doc) => {
          const obj = {
            id: doc.id,
            title: doc.data().title,
            location: doc.data().location,
            price: doc.data().price,
            bedroom: doc.data().bedroom,
            bathroom: doc.data().bathroom,
            kitchen: doc.data().kitchen,
            squaremeters: doc.data().squaremeters,
            type: doc.data().type,
            images: doc.data().images,
            imagerefs: doc.data().imagerefs,
            features: doc.data().features,
            date: doc.data().date,
            description: doc.data().description
          }
          arrVilla.push(obj)
          console.log(obj)
        })
        villas.value = arrVilla

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const loadGallery = async () => {
      isLoading.value = true
      try {
        const gallerySnapshot = await getDocs(collection(db, 'gallery'))
        const arrGallery: any[] = []
        gallerySnapshot.forEach((doc) => {
          const obj = {
            id: doc.id,
            image: doc.data().image,
            imagerefs: doc.data().imagerefs,
            description: doc.data().description
          }
          arrGallery.push(obj)
          console.log(obj)
        })
        gallery.value = arrGallery

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }

    const deleteLands = async ({ id, imagerefs }: any) => {
      isLoading.value = true
      try {
        for (let index = 0; index < imagerefs.length; index++) {
          const imageRef = Ref(storage, 'images/land/' + imagerefs[index])

          await deleteObject(imageRef)
        }

        //  delete property with id
        await deleteDoc(doc(db, 'properties', id))

        const index = lands.value.findIndex((land) => {
          return id == land.id
        })
        lands.value.splice(index, 1)

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const deleteApartments = async ({ id, imagerefs }: any) => {
      isLoading.value = true
      try {
        for (let index = 0; index < imagerefs.length; index++) {
          const imageRef = Ref(storage, 'images/apartment/' + imagerefs[index])

          await deleteObject(imageRef)
        }

        //  delete property with id
        await deleteDoc(doc(db, 'properties', id))

        const index = apartments.value.findIndex((apartment) => {
          return id == apartment.id
        })
        apartments.value.splice(index, 1)

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const deleteBungalow = async ({ id, imagerefs }: any) => {
      isLoading.value = true
      try {
        for (let index = 0; index < imagerefs.length; index++) {
          const imageRef = Ref(storage, 'images/bungalow/' + imagerefs[index])

          await deleteObject(imageRef)
        }

        //  delete property with id
        await deleteDoc(doc(db, 'properties', id))

        const index = bungalows.value.findIndex((bungalow) => {
          return id == bungalow.id
        })
        bungalows.value.splice(index, 1)

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const deleteVilla = async ({ id, imagerefs }: any) => {
      isLoading.value = true
      try {
        for (let index = 0; index < imagerefs.length; index++) {
          const imageRef = Ref(storage, 'images/villa/' + imagerefs[index])

          await deleteObject(imageRef)
        }

        //  delete property with id
        await deleteDoc(doc(db, 'properties', id))

        const index = villas.value.findIndex((vila) => {
          return id == vila.id
        })
        villas.value.splice(index, 1)

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const deleteProject = async ({ id, imagerefs }: any) => {
      isLoading.value = true
      try {
        for (let index = 0; index < imagerefs.length; index++) {
          const imageRef = Ref(storage, 'images/project/' + imagerefs[index])

          await deleteObject(imageRef)
        }

        //  delete property with id
        await deleteDoc(doc(db, 'properties', id))

        const index = projects.value.findIndex((project) => {
          return id == project.id
        })
        projects.value.splice(index, 1)

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }
    const deleteGallery = async ({ id, imagerefs }: any) => {
      isLoading.value = true
      try {
        const imageRef = Ref(storage, 'images/gallery/' + imagerefs)

        await deleteObject(imageRef)

        //  delete property with id
        await deleteDoc(doc(db, 'gallery', id))

        const index = gallery.value.findIndex((g) => {
          return id == g.id
        })
        gallery.value.splice(index, 1)

        isLoading.value = false
      } catch (error: any) {
        isLoading.value = false

        uploadError.value.message = error.response.data.err.message
        uploadError.value.state = true

        setTimeout(() => {
          uploadError.value.message = ''
          uploadError.value.state = false
        }, 3000)
      }
    }

    return {
      isLoading,
      uploadError,
      deleteApartments,
      deleteBungalow,
      createLand,
      deleteVilla,
      createGallery,
      loadApartments,
      lands,
      deleteGallery,
      gallery,
      deleteLands,
      loadLands,
      apartments,
      deleteProject,
      projects,
      villas,
      bungalows,
      loadGallery,
      loadProjects,
      loadBungalows,
      loadVillas,
      createApartment,
      createVilla,
      createProject,
      createBungalow
    }
  },
  {
    // persist: true
  }
)
