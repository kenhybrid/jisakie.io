import 'package:app/provider/auth_provider.dart';
import 'package:app/screens/auth/login/login_screen.dart';
import 'package:app/screens/auth/register/register_screen.dart';
import 'package:app/screens/auth/reset/reset_screen.dart';
import 'package:app/screens/home/home_screen.dart';
import 'package:app/screens/welcome/welcome_screen.dart';
import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<MyAuthProvider>(
          create: (_) => MyAuthProvider(), // Your AuthProvider instance
        ),
        // Add other providers here if needed
        // ChangeNotifierProvider<AnotherProvider>(
        //   create: (_) => AnotherProvider(),
        // ),
      ],
      child: MaterialApp(
        title: 'jisakie.io',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'Outfit',
          colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFFb34143)),
          useMaterial3: false,
        ),
        // home: const HomeScreen(),
        initialRoute: '/',
        routes: <String, WidgetBuilder>{
          // When navigating to the "/" route, build the FirstScreen widget.
          '/': (context) => const HomeScreen(),
          // When navigating to the "/second" route, build the SecondScreen widget.
          '/welcome': (context) => const WelcomeScreen(),
          '/register': (context) => const RegisterScreen(),
          '/login': (context) => const LoginScreen(),
          '/reset': (context) => const ResetScreen(),
        },
      ),
    );
  }
}
