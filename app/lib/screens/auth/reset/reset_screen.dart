import 'package:app/provider/auth_provider.dart';
import 'package:app/widgets/custom_filled_button.dart';
import 'package:app/widgets/custom_icon_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ResetScreen extends StatefulWidget {
  const ResetScreen({super.key});

  @override
  State<ResetScreen> createState() => _ResetScreenState();
}

class _ResetScreenState extends State<ResetScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isObscure = true;

  void _dismissKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  String? _validateEmail(String value) {
    String pattern = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      return 'Enter a valid email';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<MyAuthProvider>(context);

    return Scaffold(
      body: GestureDetector(
        onTap: () {
          _dismissKeyboard(context);
        },
        child: Center(
          child: SingleChildScrollView(
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.manual,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/reset-page.png",
                    height: 280,
                  ),
                  const SizedBox(
                    height: 20,
                  ),

                  const Text(
                    "Reset",
                    style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                  ),
                  const Text(
                    "Let us help you recover your accounts password.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black54,
                        fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TextFormField(
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                            labelText: 'Email',
                            prefixIcon: Icon(Icons.email_outlined),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            // border: OutlineInputBorder(),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your email';
                            }

                            return _validateEmail(value);
                          },
                        ),
                        const SizedBox(height: 25),
                        SizedBox(
                          width: double.infinity,
                          height: 43,
                          child: CustomFilledButton(
                              text: "Submit",
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  // TODO: Handle form submission (e.g., perform login)
                                  String email = _emailController.text.trim();

                                  // authProvider.signInWithEmailAndPassword(
                                  //     email, password);

                                  Navigator.pushNamed(context, "/login");
                                }
                              }),
                        ),
                      ],
                    ),
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: [
                  //     const Text("You forgot your password?"),
                  //     TextButton(
                  //         onPressed: () {
                  //           Navigator.pushNamed(context, "/reset");
                  //         },
                  //         child: const Text(
                  //           "Reset",
                  //           textAlign: TextAlign.left,
                  //           style: TextStyle(
                  //               color: Color(0xFFb34143),
                  //               fontSize: 18,
                  //               fontWeight: FontWeight.bold),
                  //         ))
                  //   ],
                  // ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
