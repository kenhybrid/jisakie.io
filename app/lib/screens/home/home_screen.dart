import 'package:app/provider/auth_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<MyAuthProvider>(context);
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            const Text("Home"),
            TextButton(
                onPressed: () {
                  if (kDebugMode) {
                    print(authProvider.user);
                    Navigator.pushNamed(context, "/welcome");
                  }
                },
                child: const Text("Welcome page"))
          ],
        ),
      ),
    );
  }
}
