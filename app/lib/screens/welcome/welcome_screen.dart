import 'package:app/widgets/custom_filled_button.dart';
import 'package:app/widgets/custom_outline_button.dart';

import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({super.key});

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/rent.png",
                  height: 280,
                ),

                const Text(
                  "Jisakie.io",
                  style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                ),

                const Text(
                  "welcome to Jisakie.io where property search has been made convenient. Search for a property at your own confort.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black54,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 40,
                ),
                // custom buttons
                SizedBox(
                  width: double.infinity,
                  height: 43,
                  child: CustomFilledButton(
                      text: "Get started",
                      onPressed: () {
                        Navigator.pushNamed(context, "/register");
                      }),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 43,
                  child: CustomOutlineButton(
                      text: "login",
                      onPressed: () {
                        Navigator.pushNamed(context, "/login");
                      }),
                ),

                // Row(
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     const Text("Already have an account?"),
                //     TextButton(
                //         onPressed: () {
                //           Navigator.pushNamed(context, "/login");
                //         },
                //         child: const Text(
                //           "Sign In",
                //           textAlign: TextAlign.left,
                //           style: TextStyle(
                //               color: Color(0xFFb34143),
                //               fontSize: 18,
                //               fontWeight: FontWeight.bold),
                //         ))
                //   ],
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
