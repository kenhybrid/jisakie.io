import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String icon;
  const CustomIconButton(
      {super.key, required this.onPressed, required this.icon});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.all(5),
        shape: const CircleBorder(side: BorderSide.none),
        elevation: 1,
        side: const BorderSide(
          width: 1,
          color: Color(0xFFb34143),
        ),
        backgroundColor: Colors.white,
      ),
      child: Image.asset(
        icon,
        height: 30,
      ),
    );
  }
}


// import 'package:flutter/material.dart';

// class CustomIconButton extends StatelessWidget {
//   final VoidCallback onPressed;
//   final IconData icon;
//   const CustomIconButton(
//       {super.key, required this.onPressed, required this.icon});

//   @override
//   Widget build(BuildContext context) {
//     return IconButton(
//       onPressed: onPressed,
//       style: IconButton.styleFrom(
//         iconSize: 30,
//         foregroundColor: Colors.black,
//       ),
//       icon: Icon(icon),
//     );
//   }
// }
