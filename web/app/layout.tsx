import "./globals.css";
import type { Metadata } from "next";
import { Outfit } from "next/font/google";

const outfit = Outfit({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Jisakie",
  description:
    "We are a property search and handyman service company that ensures you search for a rental property and get home services at the comfort of your phone. Its easy, its fast and way convenient, set up an account with us today and let us find the property of your dreams.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={outfit.className}>{children}</body>
    </html>
  );
}
